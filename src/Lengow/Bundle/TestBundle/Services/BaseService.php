<?php
namespace Lengow\Bundle\TestBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use Doctrine\ORM\EntityManager;

/**
 * BaseService
 *
 * @author SealandMan
 *        
 */
class BaseService
{

    /**
     *
     * @var Container
     */
    protected $container;

    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var Logger
     */
    protected $logger;

    const LOG_ORDER_FORMATTER = "[%datetime%] %level_name%: %message%\n";

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->initDoctrine();
    }

    /**
     * Set Logger
     *
     * @param Logger $logger            
     */
    public function setLogger(Logger $logger)
    {
        $this->initLogger($logger);
    }

    /**
     * Init Doctrine Entities
     */
    protected function initDoctrine()
    {
        $this->em = $this->container->get('doctrine')->getManager();
    }

    /**
     * Init Log File
     * 
     * @param Logger $logger            
     */
    protected function initLogger(Logger $logger)
    {
        $fichierLog = $this->container->getParameter('log_orders');
        $handler = new StreamHandler($fichierLog);
        $handler->setFormatter(new LineFormatter(self::LOG_ORDER_FORMATTER));
        
        $this->logger = $logger;
        $this->logger->pushHandler($handler);
    }
}