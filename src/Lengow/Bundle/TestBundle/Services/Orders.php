<?php
namespace Lengow\Bundle\TestBundle\Services;

use Monolog\Logger;
use Lengow\Bundle\TestBundle\Entity\LengowOrder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Service to get and save on database orders from XML
 * 
 * @author SealandMan
 *        
 */
class Orders extends BaseService
{
    /**
     *
     * @var \SimpleXMLElement
     */
    private $xml;

    /**
     *
     * @var string
     */
    private $urlOrders;

    /**
     * Set Url Orders XML
     * 
     * @param string $urlOrders            
     */
    public function setUrlOrders($urlOrders)
    {
        $this->urlOrders = $urlOrders;
    }
    
    /**
     * Save orders and products from XML
     */
    public function saveXML()
    {
        $this->logger->info("Save XML in Database...");
        $this->em->beginTransaction();
        try {
            $this->getXML();
            $orders = $this->xml->orders[0];
            
            foreach ($orders as $order) {
                $refId = $order->order_refid->__toString();
                if (($orderExist = $this->checkIsNew($refId)) === false) {
                    $lengowOrder = LengowOrder::newInstance($order);
                    $this->em->persist($lengowOrder);
                } else {
                    $this->logger->warning("Warning : This order (RefId=$refId) exists ==> " . print_r($orderExist->__toString(), true));
                }
                
            }
            $this->em->flush();
            $this->em->commit();
            $this->logger->info("Successful saving Orders and theirs products");
        } catch (\Exception $e) {
            $this->logger->error("Error saving XML in Database : " . print_r($e->getMessage(), true));
            $this->em->rollback();
        }
    }
    
    /**
     * Check if order doesn't exist
     * 
     * @param string $refId
     * @return LengowOrder
     */
    public function checkIsNew($refId)
    {
        $order = $this->em->getRepository("TestBundle:LengowOrder")->findOneBy([
            'idRef' => $refId
        ]);
        
        return is_null($order) ? false : $order;
    }
    
    /**
     * Save one order
     * @param unknown $order
     */
    public function saveOrder($order)
    {
        $this->logger->info("Save Order in Database...");
        $this->em->beginTransaction();
        try {
            $order->setMarketPlace("LengowTest");
            $order->setIdFlux(rand(1200, mt_getrandmax()));
            $order->setIdRef(rand(3000, mt_getrandmax()) . "--" . rand(1000, 3000));
            $order->setPurchaseDatetime(new \DateTime("now"));
            
            $products = $order->getProducts();
            $amount = 0.0;
            foreach ($products as $product) {
                $product->calculatePrice();
                $product->setOrder($order);
                $amount = bcadd($amount, $product->getPrice(), 2);
            }
            
            $order->setAmount(floatval($amount));
            $order->setCommission(5.50);

            $this->em->persist($order);
            
            $this->em->flush();
            $this->em->commit();
            $this->logger->info("Successful saving Order and theirs products");
        } catch (\Exception $e) {
            $this->logger->error("Error saving Order in Database : " . print_r($e->getMessage(), true));
            $this->em->rollback();
            
            return false;
        }
        
        return true;
    }
    
    public function getOrders($format, $idOrder = "all")
    {
        $order = null;
        if($idOrder == "all") {
            $order = $this->em->getRepository('TestBundle:LengowOrder')->findAll();
        } else {
            $order = $this->em->getRepository("TestBundle:LengowOrder")->findOneBy([
                'idRef' => $idOrder
            ]);
        }
        
        $serializer = $this->container->get('serializer');
        return $serializer->serialize($order, $format);
    }
    
    /**
     * Get XML from URL
     */
    private function getXML()
    {
        $this->logger->info("Get XML from... " . $this->urlOrders);
        try {
            $this->xml = new \SimpleXMLElement($this->urlOrders, 0, true);
            $this->logger->info("Successful getting XML from URL");
        } catch (\Exception $e) {
            $this->logger->error("Error getting XML from URL : " . print_r($e->getMessage(), true));
        }
    }
}