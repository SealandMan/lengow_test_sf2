<?php

namespace Lengow\Bundle\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LengowOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('products', new CollectionType(), [
                'label_attr' => ['class' => 'hidden'],
                'type' => new LengowProductType(),
                'options' => [
                    'attr' => ['class' => 'product'],
                    
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ])
            ->add('save', new SubmitType(), [
                'attr' => [
                    'class' => 'btn btn-primary input-sm'
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lengow\Bundle\TestBundle\Entity\LengowOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lengow_bundle_testbundle_lengoworder';
    }
}
