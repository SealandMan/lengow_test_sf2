<?php
namespace Lengow\Bundle\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LengowProductType extends AbstractType
{

    /**
     *
     * @param FormBuilderInterface $builder            
     * @param array $options            
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('idRef', new TextType(), $this->getDefaultAttributeTextType())
            ->add('title', new TextType(), $this->getDefaultAttributeTextType())
            ->add('category', new TextType(), $this->getDefaultAttributeTextType())
            ->add('quantity', new NumberType(), [
                'scale' => 0,
                'invalid_message' => 'This quantity must be greater than or equal to %num%',
                'invalid_message_parameters' => [
                    '%num%' => 1
                ],
                'attr' => [
                    'class' => 'form-control input-sm'
                ]
            ])
            ->add('priceUnit', new NumberType(), [
                'scale' => 2,
                'invalid_message' => 'This quantity must be greater than to %num%',
                'invalid_message_parameters' => [
                    '%num%' => 1.5
                ],
                'attr' => [
                    'class' => 'form-control input-sm'
                ]
            ])
        ;
    }

    /**
     *
     * @param OptionsResolverInterface $resolver            
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lengow\Bundle\TestBundle\Entity\LengowProduct'
        ));
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'lengowproduct';
    }

    /**
     * 
     * @return string[][]
     */
    private function getDefaultAttributeTextType()
    {
        return [
            'attr' => [
                'class' => 'form-control input-sm'
            ]
        ];
    }
}
