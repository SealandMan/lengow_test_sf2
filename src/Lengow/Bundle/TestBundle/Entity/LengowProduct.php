<?php
namespace Lengow\Bundle\TestBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * LengowProduct
 * 
 * @GRID\Source(columns="id, idRef, title, category, priceUnit, quantity, price, order.idRef")
 */
class LengowProduct
{

    /**
     *
     * @var integer
     * 
     * @GRID\Column(visible=false)
     */
    private $id;

    /**
     *
     * @var integer
     * 
     * @GRID\Column(title="Reference ID", size="-1", filter="select")
     */
    private $idRef;

    /**
     *
     * @var string
     * 
     * @GRID\Column(title="Title", size="-1", filter="select")
     */
    private $title;

    /**
     *
     * @var string
     * 
     * @GRID\Column(title="Category", size="-1", filter="select")
     */
    private $category;

    /**
     *
     * @var integer
     * 
     * @GRID\Column(title="Quantity", size="-1", filter="select")
     */
    private $quantity;

    /**
     *
     * @var float
     * 
     * @GRID\Column(title="Price", size="-1", filter="select")
     */
    private $price;

    /**
     *
     * @var float
     * 
     * @GRID\Column(title="Unit Price", size="-1", filter="select")
     */
    private $priceUnit;

    /**
     *
     * @var LengowOrder
     * 
     * @GRID\Column(field="order.idRef", title="Order Reference ID", size="-1", filter="select")
     */
    private $order;

    /**
     * Constructor
     *
     * @param \SimpleXMLElement $product            
     * @param LengowOrder $order            
     */
    public function __construct(\SimpleXMLElement $product = null, LengowOrder $order = null)
    {
        if (!is_null($product)) {
            $this->idRef = $product->sku->__toString();
            $this->title = $product->title->__toString();
            $this->category = $product->category->__toString();
            $this->quantity = $product->quantity->__toString();
            $this->priceUnit = $product->price_unit->__toString();
            $this->price = $product->price->__toString();
        }
        
        if (! is_null($order)) {
            $this->order = $order;
        }
    }

    /**
     * Get an instance of LengowProduct
     *
     * @param \SimpleXMLElement $product            
     * @param LengowOrder $order            
     * @return \Lengow\Bundle\TestBundle\Entity\LengowProduct
     */
    public static function newInstance(\SimpleXMLElement $product, LengowOrder $order = null)
    {
        return new self($product, $order);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idRef
     *
     * @param string $idRef            
     * @return LengowProduct
     */
    public function setIdRef($idRef)
    {
        $this->idRef = $idRef;
        
        return $this;
    }

    /**
     * Get idRef
     *
     * @return string
     */
    public function getIdRef()
    {
        return $this->idRef;
    }

    /**
     * Set title
     *
     * @param string $title            
     * @return LengowProduct
     */
    public function setTitle($title)
    {
        $this->title = $title;
        
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set category
     *
     * @param string $category            
     * @return LengowProduct
     */
    public function setCategory($category)
    {
        $this->category = $category;
        
        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity            
     * @return LengowProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price            
     * @return LengowProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;
        
        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceUnit
     *
     * @param float $priceUnit            
     * @return LengowProduct
     */
    public function setPriceUnit($priceUnit)
    {
        $this->priceUnit = $priceUnit;
        
        return $this;
    }

    /**
     * Get priceUnit
     *
     * @return float
     */
    public function getPriceUnit()
    {
        return $this->priceUnit;
    }

    /**
     * Set order
     *
     * @param LengowOrder $order            
     * @return LengowProduct
     */
    public function setOrder(LengowOrder $order)
    {
        $this->order = $order;
        
        return $this;
    }

    /**
     * Get order
     *
     * @return LengowOrder
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        $string = "IdRef=%s/Title=%s/Category=%s/Quantity=%s/PriceUnit=%s/Price=%s";
        return sprintf(
            $string,
            $this->idRef,
            $this->title,
            $this->category,
            $this->quantity,
            $this->priceUnit,
            $this->price
        );
    }
    
    /**
     * Calculate the price
     */
    public function calculatePrice()
    {
        $price = bcmul($this->quantity, $this->priceUnit, 2);
        $this->price = floatval($price);
    }
}
