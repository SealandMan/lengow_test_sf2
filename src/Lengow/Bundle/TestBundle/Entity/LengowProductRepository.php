<?php

namespace Lengow\Bundle\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * LengowProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LengowProductRepository extends EntityRepository
{
}
