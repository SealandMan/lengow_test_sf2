<?php
namespace Lengow\Bundle\TestBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * LengowOrder
 * 
 * @GRID\Source(columns="id, marketplace, idFlux, idRef, purchaseDatetime, amount, commission, products.title, products.quantity, products.price")
 */
class LengowOrder
{

    /**
     *
     * @var integer
     * 
     * @GRID\Column(visible=false)
     */
    private $id;

    /**
     *
     * @var string
     * 
     * @GRID\Column(title="Market Place", size="-1", filter="select")
     */
    private $marketplace;

    /**
     *
     * @var integer
     * 
     * @GRID\Column(title="Flux ID", size="-1", filter="select")
     */
    private $idFlux;

    /**
     *
     * @var string
     * 
     * @GRID\Column(title="Reference ID", size="-1", filter="select")
     */
    private $idRef;

    /**
     *
     * @var \DateTime
     * 
     * @GRID\Column(title="Purchase Datetime", type="datetime", format="d-m-Y H:i", timezone="Europe/Paris", size="-1", filter="select")
     */
    private $purchaseDatetime;

    /**
     *
     * @var float
     * 
     * @GRID\Column(title="Amount", size="-1", filter="select")
     */
    private $amount;

    /**
     *
     * @var float
     * 
     * @GRID\Column(title="Commission", size="-1", filter="select")
     */
    private $commission;

    /**
     *
     * @var mixed
     * 
     * @GRID\Column(field="products.title", title="Product Title", joinType="inner", filter="select")
     * @GRID\Column(field="products.quantity", title="Product Quantity", joinType="inner", filter="select")
     * @GRID\Column(field="products.price", title="Product Price", joinType="inner", filter="select")
     */
    private $products;

    /**
     * Constructor
     *
     * @param \SimpleXMLElement $order            
     */
    public function __construct(\SimpleXMLElement $order = null)
    {
        if (!is_null($order)) {
            $this->marketplace = $order->marketplace->__toString();
            $this->idFlux = $order->idFlux->__toString();
            $this->idRef = $order->order_refid->__toString();
            $dateP = $order->order_purchase_date->__toString();
            $timeP = $order->order_purchase_heure->__toString();
            $this->purchaseDatetime = $this->getDatetime($dateP, $timeP);
            $this->amount = floatval($order->order_amount->__toString());
            $this->commission = floatval($order->order_commission->__toString());
            
            $this->addProducts($order->cart->products[0]);
        }
    }

    /**
     * Get an instance of LengowOrder
     *
     * @param \SimpleXMLElement $order            
     * @return \Lengow\Bundle\TestBundle\Entity\LengowOrder
     */
    public static function newInstance(\SimpleXMLElement $order)
    {
        return new self($order);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace            
     * @return LengowOrder
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;
        
        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux            
     * @return LengowOrder
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;
        
        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set idRef
     *
     * @param string $idRef            
     * @return LengowOrder
     */
    public function setIdRef($idRef)
    {
        $this->idRef = $idRef;
        
        return $this;
    }

    /**
     * Get idRef
     *
     * @return string
     */
    public function getIdRef()
    {
        return $this->idRef;
    }

    /**
     * Set purchaseDatetime
     *
     * @param \DateTime $purchaseDatetime            
     * @return LengowOrder
     */
    public function setPurchaseDatetime(\Datetime $purchaseDatetime)
    {
        $this->purchaseDatetime = $purchaseDatetime;
        
        return $this;
    }

    /**
     * Get purchaseDatetime
     *
     * @return \DateTime
     */
    public function getPurchaseDatetime()
    {
        return $this->purchaseDatetime;
    }

    /**
     * Set amount
     *
     * @param float $amount            
     * @return LengowOrder
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        
        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set commission
     *
     * @param float $commission            
     * @return LengowOrder
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
        
        return $this;
    }

    /**
     * Get commission
     *
     * @return float
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set products
     *
     * @param mixed $products            
     * @return LengowOrder
     */
    public function setProducts($products)
    {
        $this->products = $products;
        
        return $this;
    }

    /**
     * Get products
     *
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Transform Products XML to Entities LengowProduct
     * 
     * @param \SimpleXMLElement $products            
     */
    public function addProducts(\SimpleXMLElement $products)
    {
        foreach ($products as $product) {
            $lengowProduct = LengowProduct::newInstance($product);
            $lengowProduct->setOrder($this);
            $this->products[] = $lengowProduct;
        }
    }
    
    /**
     * Remove Lengow Product
     * @param LengowProduct $product
     */
    public function removeProduct(LengowProduct $product)
    {
        $this->products->removeElement($product);
    }
    

    /**
     * @return string
     */
    public function __toString()
    {
        $string = "Marketplace=%s/IdRef=%s/IdFlux=%s/PurchaseDatetime=%s/Amount=%s/Commission=%s";
        return sprintf(
            $string, 
            $this->marketplace, 
            $this->idRef, 
            $this->idFlux, 
            is_null($this->purchaseDatetime) ? "" : $this->purchaseDatetime->format("d-m-Y H:i:s"), 
            $this->amount, 
            $this->commission
        );
    }

    /**
     * Get Datetime from a date and a time
     *
     * @param string $date            
     * @param string $time            
     * @return \DateTime
     */
    private function getDatetime($date, $time)
    {
        $format = "Y-m-d H:i:s";
        if (empty($date)) {
            return null;
        }
        if (empty($time)) {
            $format = "Y-m-d ";
        }
        return \DateTime::createFromFormat($format, $date . " " . $time);
    }
}
