<?php
namespace Lengow\Bundle\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Request;
use Lengow\Bundle\TestBundle\Form\LengowOrderType;
use Lengow\Bundle\TestBundle\Entity\LengowOrder;
use Symfony\Component\HttpFoundation\Response;
use APY\DataGridBundle\Grid\Action\RowAction;

class OrdersController extends Controller
{

    const SERVICE_ORDERS = 'orders';

    /**
     * Index Action
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        //Save last orders
        $serviceLengow = $this->get(self::SERVICE_ORDERS);
        $serviceLengow->saveXML();
        
        //Manage Grid
        $source = new Entity("TestBundle:LengowOrder");
        $grid = $this->get('grid');
        $grid->setSource($source);        
        
        return $grid->getGridResponse('TestBundle:Orders:index.html.twig');
    }
    
    /**
     * NewOrder Action
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newOrderAction(Request $request)
    {
        $newOrder = new LengowOrder();
        $form = $this->createForm(new LengowOrderType(), $newOrder);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $order = $form->getData();
            
            $serviceLengow = $this->get(self::SERVICE_ORDERS);
            $session = $request->getSession();
            $retour = $serviceLengow->saveOrder($order);
            
            if ($retour) {
                $session->getFlashBag()->add('success', 'Order saved with success');
                return $this->redirectToRoute("orders_index");
            }
            
            $session->getFlashBag()->add('error', 'Failed to save order in database!');
        }
        
        
        return $this->render("TestBundle:Orders:new.html.twig", [
            "form" => $form->createView()
        ]);
    }
    
    /**
     * GetOrders Action
     * @param Request $request
     * @param int $idOrder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getOrdersAction(Request $request, $idOrder)
    {
        $serviceLengow = $this->get(self::SERVICE_ORDERS);
        $format = $request->getRequestFormat();
        $order = $serviceLengow->getOrders($format, $idOrder);
        
        return $this->getApiResponse($order, $format);
    }
    
    private function getApiResponse($orders, $format)
    {
        $response = new Response();
        $response->setContent($orders, $format);
        if ($format == 'json') {
            $response->headers->set('Content-Type', 'application\json');
        }
        if ($format == 'yml') {
            $response->headers->set('Content-Type', 'text\yaml');
        }
        
        
        return $response;
    }
}
